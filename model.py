from __future__ import division
import os
import time
from glob import glob
import tensorflow as tf
import numpy as np
# from six.moves import xrange

from ops import *
from utils import *
import Definitions
import Datasets

class DCGAN(object):
    def __init__(self, sess, image_size=108, is_crop=True,
                 batch_size=64, sample_size=64, image_shape=[64, 64, 3],
                 y_dim=None, z_dim=100, gf_dim=16, df_dim=16,
                 gfc_dim=1024, dfc_dim=1024, c_dim=1, dataset_name='default',
                 checkpoint_dir=None, learning_rate=None, beta1=None):
        """

        Args:
            sess: TensorFlow session
            batch_size: The size of batch. Should be specified before training.
            y_dim: (optional) Dimension of dim for labels. [None]
            z_dim: (optional) Dimension of dim for Z. [100]
            gf_dim: (optional) Dimension of gen filters in first conv layer. [64]
            df_dim: (optional) Dimension of discrim filters in first conv layer. [64]
            gfc_dim: (optional) Dimension of gen untis for for fully connected layer. [1024]
            dfc_dim: (optional) Dimension of discrim units for fully connected layer. [1024]
            c_dim: (optional) Dimension of image color. [3]
        """
        self.sess = sess
        self.is_crop = is_crop
        self.batch_size = batch_size
        self.image_size = image_size
        self.sample_size = sample_size
        self.image_shape = image_shape
        self.image_height = image_shape[0]
        self.image_width = image_shape[1]

        self.learning_rate = learning_rate
        self.beta1 = beta1

        self.labels_dim = y_dim
        self.z_dim = z_dim
        self.year_dim = 40

        self.gf_dim = gf_dim
        self.df_dim = df_dim

        self.gfc_dim = gfc_dim
        self.dfc_dim = dfc_dim

        self.c_dim = c_dim

        # batch normalization : deals with poor initialization helps gradient flow
        self.d_bn1 = batch_norm(name='d_bn1')
        self.d_bn2 = batch_norm(name='d_bn2')
        self.d_bn3 = batch_norm(name='d_bn3')

        self.ye_bn1 = batch_norm(name='ye_bn1')
        self.ye_bn2 = batch_norm(name='ye_bn2')
        self.ye_bn3 = batch_norm(name='ye_bn3')

        self.di_bn1 = batch_norm(name='di_bn1')
        self.di_bn2 = batch_norm(name='di_bn2')
        self.di_bn3 = batch_norm(name='di_bn3')

        self.g_bn0 = batch_norm(name='g_bn0')
        self.g_bn1 = batch_norm(name='g_bn1')
        self.g_bn2 = batch_norm(name='g_bn2')
        self.g_bn3 = batch_norm(name='g_bn3')

        self.dataset_name = dataset_name
        self.checkpoint_dir = checkpoint_dir

        # Build Model

        if self.labels_dim:
            self.labels = tf.placeholder(tf.float32, [self.batch_size, self.labels_dim], name='labels')

        self.images = tf.placeholder(tf.float32, [self.batch_size] + self.image_shape,
                                    name='real_images')

        self.z = tf.placeholder(tf.float32, [None, self.z_dim + self.year_dim],
                                name='z')

        self.z_sum = tf.histogram_summary("z", self.z)
        #
        # if self.labels_dim:
        #     self.reconstruction = self.decoder(self.z, self.labels)
        #     self.sigmoided_encoding_real, self.encoding_real = self.encoder(self.images, self.labels, reuse=False)
        #
        #     self.sampler = self.sampler(self.z, self.labels)
        #     self.sigmoided_encoding_generated, self.encoding_generated = self.encoder(self.reconstruction, self.labels, reuse=True)
        # else:
        #     self.reconstruction = self.decoder(self.z)
        #     self.encoding_real, self.sigmoided_encoding_real = self.encoder(self.images)
        #
        #     self.sampler = self.sampler(self.z)
        #     self.encoding_generated, self.sigmoided_encoding_generated = self.encoder(self.reconstruction, reuse=True)

        # My part here #
        with tf.variable_scope("Year_Encoder", reuse=None):
            self.encoded_year = self.year_encoder(self.labels)

        if self.labels_dim:
            _, self.encoded_images = self.encoder(self.images, self.labels, reuse=False)
            self.encoded_images = tf.concat(1, [self.encoded_images, self.encoded_year])

            self.reconstructed_images = self.decoder(self.encoded_images, self.labels)
            # self.encoded_images_year = \
            #     tf.concat(1,
            #               [tf.slice(self.encoded_images, [0, 0], [-1, self.z_dim - self.year_dim]), self.encoded_year])
            #
            # self.encoded_images = tf.cond(tf.less(tf.random_uniform(shape=()), 0.5),
            #                               lambda: self.encoded_images,
            #                               lambda: self.encoded_images_year)
            # self.encoded_images = tf.reshape(self.encoded_images, [self.batch_size, self.z_dim])

            self.sampler = self.sampler(self.z, self.labels)
            # _, self.encoded_reconstructions = self.encoder(self.sampler, self.labels, reuse=True)
        else:
            _, self.encoded_images = self.encoder(self.images, reuse=False)
            self.reconstructed_images = self.decoder(self.encoded_images)

            self.sampler = self.sampler(self.z)
            # _, self.encoded_reconstructions = self.encoder(self.sampler, reuse=True)

        self.sobel_real = self.sobel(self.images)
        self.sobel_enc = self.sobel(self.reconstructed_images)
        self.sobel_gen = self.sobel(self.sampler)

        # Want to minimize this...
        self.reconstruction_loss_actual = tf.reduce_mean(tf.square(tf.sub(self.images, self.reconstructed_images)))
        self.reconstruction_loss_sobel = tf.mul(tf.reduce_mean(tf.square(tf.sub(self.sobel_real, self.sobel_enc))), 0.0015)
        self.reconstruction_loss = tf.add(self.reconstruction_loss_actual, self.reconstruction_loss_sobel)

        with tf.variable_scope("Discriminator", reuse=None) as scope:
            # Need to add a discriminator on encoded_images
            self.discriminated_real = self.discriminator(self.encoded_images)
            scope.reuse_variables()
            self.discriminated_reconstructed = self.discriminator(self.z)

        self.encoding_real_loss = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(
                self.discriminated_real, tf.ones_like(self.discriminated_real)))
        self.encoding_generated_loss = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(
                self.discriminated_reconstructed, tf.zeros_like(self.discriminated_reconstructed)))

        # Want to minimize this...
        self.encoding_loss = tf.add(self.encoding_real_loss, self.encoding_generated_loss)

        # End my part! #

        self.encoding_real_summary = tf.histogram_summary("encoding_real", self.encoded_images)
        self.encoding_generated_summary = tf.histogram_summary("encoding_generated", self.z)
        self.reconstruction_summary = tf.image_summary("encoded", self.reconstructed_images)
        self.generated_summary = tf.image_summary("generated", self.sampler)
        self.images_summary = tf.image_summary("actual", self.images)
        self.sobelx_summary = tf.image_summary("sobel_actual", self.sobel_real)
        self.sobely_summary = tf.image_summary("sobel_encoded", self.sobel_enc)
        self.sobelz_summary = tf.image_summary("sobel_generated", self.sobel_gen)

        # self.encoding_real_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.sigmoided_encoding_real, tf.ones_like(self.encoding_real)))
        # self.encoding_generated_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.sigmoided_encoding_generated, tf.zeros_like(self.encoding_generated)))
        # self.reconstruction_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.sigmoided_encoding_generated, tf.ones_like(self.encoding_generated)))

        self.encoding_real_loss_summary = tf.scalar_summary("encoding_real_loss", self.encoding_real_loss)
        self.encoding_generated_loss_summary = tf.scalar_summary("encoding_generated_loss", self.encoding_generated_loss)

        # self.encoding_loss = self.encoding_real_loss + self.encoding_generated_loss

        self.reconstruction_loss_summary = tf.scalar_summary("reconstruction_loss", self.reconstruction_loss)
        self.reconstruction_loss_actual_summary = tf.scalar_summary("reconstruction_loss_actual", self.reconstruction_loss_actual)
        self.reconstruction_loss_sobel_summary = tf.scalar_summary("reconstruction_loss_sobel", self.reconstruction_loss_sobel)
        self.encoding_loss_summary = tf.scalar_summary("encoding_loss", self.encoding_loss)

        trainable_variables = tf.trainable_variables()

        # Discriminator variables
        self.encoding_variables = [variable for variable in trainable_variables
                                   if 'Discriminator' in variable.name]

        # Non-discriminator variables
        self.reconstruction_variables = [variable for variable in trainable_variables
                                         if variable not in self.encoding_variables]

        self.d_optim = tf.train.AdamOptimizer(self.learning_rate, beta1=self.beta1).minimize(
            self.encoding_loss, var_list=self.encoding_variables)

        self.g_optim = tf.train.AdamOptimizer(self.learning_rate, beta1=self.beta1).minimize(
            self.reconstruction_loss, var_list=self.reconstruction_variables)

        self.saver = tf.train.Saver()

    def discriminator(self, images):
        with tf.variable_scope('Pass_1') as scope:
            linear_pass_1 = self.di_bn1(linear(images, self.z_dim*2, scope=scope))
            relu_pass_1 = lrelu(linear_pass_1)
        with tf.variable_scope('Pass_2') as scope:
            linear_pass_2 = self.di_bn2(linear(relu_pass_1, self.z_dim*4, scope=scope))
            relu_pass_2 = lrelu(linear_pass_2)
        with tf.variable_scope('Pass_3') as scope:
            truth = self.di_bn3(linear(relu_pass_2, 1, scope=scope))

        return truth

    def year_encoder(self, year):
        with tf.variable_scope('Year_1') as scope:
            linear_pass_1 = self.ye_bn1(linear(year, 200, scope=scope))
            relu_pass_1 = lrelu(linear_pass_1)
        with tf.variable_scope('Year_2') as scope:
            linear_pass_2 = self.ye_bn2(linear(relu_pass_1, 200, scope=scope))
            relu_pass_2 = lrelu(linear_pass_2)
        with tf.variable_scope('Year_3') as scope:
            truth = self.ye_bn3(linear(relu_pass_2, self.year_dim, scope=scope))

        return truth

    def train(self, config):
        """Train DCGAN"""
        print "1"

        data_X, data_y, data = None, None, None
        if config.dataset == 'mnist':
            data_X, data_y = self.load_mnist()
        elif config.dataset == 'portraits':
            dataset = Datasets.PortraitsMultidataset(Definitions.data_path + 'train_F.txt',
                                                     Definitions.data_path + 'test_F.txt',
                                                     im_shape=None, batch_size=self.batch_size, n_classes=None)
            sample_dataset = Datasets.PortraitsMultidataset(Definitions.data_path + 'train_F.txt',
                                                     Definitions.data_path + 'test_F.txt',
                                                     im_shape=None, batch_size=self.sample_size, n_classes=None)
            file_name, data_y, data_X = dataset.load_train()
            s_file_name, s_data_y, s_data_X = sample_dataset.load_train()
        else:
            data = glob(os.path.join("./data", config.dataset, "*.jpg"))
        # np.random.shuffle(data)

        print "2"

        self.sess.run(tf.initialize_all_variables())

        print "3"

        self.saver = tf.train.Saver()
        self.g_sum = tf.merge_summary([self.z_sum, self.generated_summary, self.images_summary, self.encoding_generated_summary,
                                       self.sobelx_summary, self.sobely_summary, self.sobelz_summary, self.reconstruction_loss_actual_summary,
                                       self.reconstruction_loss_sobel_summary, self.encoding_real_summary,
                                       self.reconstruction_summary, self.encoding_generated_loss_summary, self.reconstruction_loss_summary])
        self.encoding_real_summary = tf.merge_summary([self.z_sum, self.encoding_real_loss_summary, self.encoding_loss_summary])
        # self.g_sum = tf.merge_summary([self.encoding_generated_summary,
        #                                self.reconstruction_summary, self.encoding_generated_loss_summary, self.reconstruction_loss_summary])
        # self.encoding_real_summary = tf.merge_summary([self.encoding_real_summary, self.encoding_real_loss_summary, self.encoding_loss_summary])
        self.writer = tf.train.SummaryWriter(Definitions.log_path, self.sess.graph)

        sample_z = np.random.standard_normal(size=(8, self.z_dim + self.year_dim))
        sample_z = np.array([sample_z for _ in range(8)]).reshape([64, self.z_dim + self.year_dim])

        print "4"

        if config.dataset == 'mnist':
            sample_images = data_X[0:self.sample_size]
            sample_labels = data_y[0:self.sample_size]
        elif config.dataset == 'portraits':
            # control the input threads
            self.coord = tf.train.Coordinator()

            # start the input threads running
            self.threads = tf.train.start_queue_runners(sess=self.sess, coord=self.coord)

            sample_images, sample_labels = self.sess.run([s_data_X, s_data_y])
            sample_labels = np.reshape(sample_labels, [self.sample_size, self.labels_dim])
        else:
            sample_files = data[0:self.sample_size]
            sample = [get_image(sample_file, self.image_size, is_crop=self.is_crop) for sample_file in sample_files]
            sample_images = np.array(sample).astype(np.float32)
            sample_labels = None

        print "5"

        counter = 1
        start_time = time.time()

        if self.load(Definitions.prev_checkpoint_dir):
            print(" [*] Load SUCCESS")
        else:
            print(" [!] Load failed...")

        for epoch in xrange(config.epoch):
            if config.dataset == 'mnist':
                batch_idxs = min(len(data_X), config.train_size) // config.batch_size
            elif config.dataset == 'portraits':
                batch_idxs = min(20248, config.train_size) // config.batch_size
            else:
                data = glob(os.path.join("./data", config.dataset, "*.jpg"))
                batch_idxs = min(len(data), config.train_size) // config.batch_size

            for idx in xrange(0, batch_idxs):
                if config.dataset == 'mnist':
                    batch_images = data_X[idx*config.batch_size:(idx+1)*config.batch_size]
                    batch_labels = data_y[idx*config.batch_size:(idx+1)*config.batch_size]
                elif config.dataset == 'portraits':
                    batch_images, batch_labels = self.sess.run([data_X, data_y])
                    batch_labels = np.reshape(batch_labels,[self.batch_size, self.labels_dim])
                else:
                    batch_files = data[idx*config.batch_size:(idx+1)*config.batch_size]
                    batch = [get_image(batch_file, self.image_size, is_crop=self.is_crop) for batch_file in batch_files]
                    batch_images = np.array(batch).astype(np.float32)

                batch_z = np.random.standard_normal(size=(self.batch_size, self.z_dim + self.year_dim))

                if config.dataset == 'mnist' or config.dataset == 'portraits':
                    # Update encoding_real network
                    _, summary_str = self.sess.run([self.d_optim, self.encoding_real_summary],
                                                   feed_dict={self.images: batch_images, self.z: batch_z, self.labels: batch_labels})
                    self.writer.add_summary(summary_str, counter)

                    # Update reconstruction network
                    _, summary_str = self.sess.run([self.g_optim, self.g_sum],
                                                   feed_dict={self.images: batch_images, self.z: batch_z, self.labels: batch_labels})
                    self.writer.add_summary(summary_str, counter)

                    # # Run g_optim twice to make sure that encoding_loss does not go to zero (different from paper)
                    _, summary_str = self.sess.run([self.g_optim, self.g_sum],
                                                   feed_dict={self.images: batch_images, self.z: batch_z, self.labels: batch_labels})
                    self.writer.add_summary(summary_str, counter)
                    
                    errD_fake = self.encoding_generated_loss.eval({self.images: batch_images, self.z: batch_z, self.labels: batch_labels})
                    errD_real = self.encoding_real_loss.eval({self.images: batch_images, self.labels: batch_labels})
                    errG = self.reconstruction_loss.eval({self.images: batch_images, self.z: batch_z, self.labels: batch_labels})
                else:
                    # Update encoding_real network
                    _, summary_str = self.sess.run([self.d_optim, self.encoding_real_summary],
                                                   feed_dict={ self.images: batch_images, self.z: batch_z })
                    self.writer.add_summary(summary_str, counter)

                    # Update reconstruction network
                    _, summary_str = self.sess.run([self.g_optim, self.g_sum],
                        feed_dict={ self.z: batch_z })
                    self.writer.add_summary(summary_str, counter)

                    # Run g_optim twice to make sure that encoding_loss does not go to zero (different from paper)
                    _, summary_str = self.sess.run([self.g_optim, self.g_sum],
                        feed_dict={ self.z: batch_z })
                    self.writer.add_summary(summary_str, counter)
                    
                    errD_fake = self.encoding_generated_loss.eval({self.z: batch_z})
                    errD_real = self.encoding_real_loss.eval({self.images: batch_images})
                    errG = self.reconstruction_loss.eval({self.z: batch_z})

                counter += 1
                print("Epoch: [%2d] [%4d/%4d] time: %4.4f, encoding_loss: %.8f, reconstruction_loss: %.8f"
                    % (epoch, idx, batch_idxs,
                        time.time() - start_time, errD_fake+errD_real, errG))

                if np.mod(counter, 100) == 1:
                    if config.dataset == 'mnist':
                        samples, d_loss, g_loss = self.sess.run(
                            [self.sampler, self.encoding_loss, self.reconstruction_loss],
                            feed_dict={self.z: sample_z, self.images: sample_images, self.labels: sample_labels}
                        )
                    elif config.dataset == 'portraits':
                        # Batch size forced to 64 now...
                        # years = np.repeat(range(0, self.labels_dim - (self.labels_dim // 8), self.labels_dim // 8), 8)
                        years = np.repeat(range(0, self.labels_dim, self.labels_dim // 8), 8)

                        # One hot hack
                        years_one_hot = np.zeros((self.batch_size, self.labels_dim))
                        years_one_hot[np.arange(self.batch_size), years] = 1

                        # Replace the last part of sample_z with current value for years
                        year_vals = self.sess.run(self.encoded_year, feed_dict={self.labels: years_one_hot})
                        sample_z[:, -self.year_dim:] = year_vals
                        for row in sample_z:
                            print row

                        samples = self.sess.run(self.sampler, feed_dict={self.z: sample_z, self.labels: years_one_hot})
                    else:
                        samples, d_loss, g_loss = self.sess.run(
                            [self.sampler, self.encoding_loss, self.reconstruction_loss],
                            feed_dict={self.z: sample_z, self.images: sample_images}
                        )
                    save_images(samples, [8, 8],
                                './samples/train_%s_%s.png' % (epoch, idx))
                    # print("[Sample] encoding_loss: %.8f, reconstruction_loss: %.8f" % (d_loss, g_loss))

                if np.mod(counter, 500) == 2:
                    self.save(config.checkpoint_dir, counter)

    def encoder(self, image, y=None, reuse=False):
        with tf.variable_scope('encoder', reuse=reuse):
            # if reuse:
            #     tf.get_variable_scope().reuse_variables()

            if not self.labels_dim:
                h0 = lrelu(conv2d(image, self.df_dim, name='d_h0_conv'))
                h1 = lrelu(self.d_bn1(conv2d(h0, self.df_dim*2, name='d_h1_conv')))
                h2 = lrelu(self.d_bn2(conv2d(h1, self.df_dim*4, name='d_h2_conv')))
                h3 = lrelu(self.d_bn3(conv2d(h2, self.df_dim*8, name='d_h3_conv')))
                h4 = linear(tf.reshape(h3, [self.batch_size, -1]), self.z_dim, 'd_h3_lin')

                return tf.nn.sigmoid(h4), h4
            else:
                yb = tf.reshape(y, [self.batch_size, 1, 1, self.labels_dim])
                x = conv_cond_concat(image, yb)

                h0 = lrelu(conv2d(x, self.c_dim + self.labels_dim, name='d_h0_conv'))
                h0 = conv_cond_concat(h0, yb)

                h1 = lrelu(self.d_bn1(conv2d(h0, self.df_dim + self.gf_dim, name='d_h1_conv')))
                h1 = tf.reshape(h1, [self.batch_size, -1])
                h1 = tf.concat(1, [h1, y])

                h2 = lrelu(self.d_bn2(linear(h1, self.dfc_dim, 'd_h2_lin')))
                h2 = tf.concat(1, [h2, y])

                h3 = linear(h2, self.z_dim, 'd_h3_lin')

                return tf.nn.sigmoid(h3), h3

    def decoder(self, z, y=None):
        with tf.variable_scope("Generator"):
            if not self.labels_dim:
                # project `z` and reshape
                self.z_, self.h0_w, self.h0_b = linear(z, self.gf_dim*8*4*4, 'g_h0_lin', with_w=True)

                self.h0 = tf.reshape(self.z_, [-1, 4, 4, self.gf_dim * 8])
                h0 = lrelu(self.g_bn0(self.h0))

                self.h1, self.h1_w, self.h1_b = deconv2d(h0,
                    [self.batch_size, 8, 8, self.gf_dim*4], name='g_h1', with_w=True)
                h1 = lrelu(self.g_bn1(self.h1))

                h2, self.h2_w, self.h2_b = deconv2d(h1,
                    [self.batch_size, 16, 16, self.gf_dim*2], name='g_h2', with_w=True)
                h2 = lrelu(self.g_bn2(h2))

                h3, self.h3_w, self.h3_b = deconv2d(h2,
                    [self.batch_size, 32, 32, self.gf_dim*1], name='g_h3', with_w=True)
                h3 =lrelu(self.g_bn3(h3))

                h4, self.h4_w, self.h4_b = deconv2d(h3,
                    [self.batch_size, 64, 64, 3], name='g_h4', with_w=True)


                return tf.nn.tanh(h4)
            else:
                # yb = tf.expand_dims(tf.expand_dims(labels, 1),2)
                yb = tf.reshape(y, [self.batch_size, 1, 1, self.labels_dim])
                z = tf.concat(1, [z, y])

                h0 = lrelu(self.g_bn0(linear(z, self.gfc_dim, 'g_h0_lin')))
                h0 = tf.concat(1, [h0, y])

                h1 = lrelu(self.g_bn1(linear(h0, self.gf_dim * 4 * 32 * 32, 'g_h1_lin')))
                h1 = tf.reshape(h1, [self.batch_size, 32, 32, self.gf_dim * 4])
                h1 = conv_cond_concat(h1, yb)

                h2 = deconv2d(h1, [self.batch_size, 64, 64, self.gf_dim], name='g_h2')
                h2 = self.g_bn2(h2)
                h2 = lrelu(h2)
                h2 = conv_cond_concat(h2, yb)

                # h2 = tf.nn.relu(self.g_bn2(linear(h1, self.gf_dim * 4 * 32 * 32, 'g_h2_lin')))
                # h2 = tf.reshape(h2, [self.batch_size, 64, 64, self.gf_dim * 1])
                # h2 = conv_cond_concat(h2, yb)

                # h3 = deconv2d(h2, [self.batch_size, 64, 64, self.gf_dim], name='g_h3')
                # h3 = self.g_bn3(h3)
                # h3 = tf.nn.relu(h3)
                # h3 = conv_cond_concat(h3, yb)

                return tf.nn.sigmoid(deconv2d(h2, [self.batch_size, 128, 128, 1], name='g_h3'))

    def sampler(self, z, y=None):
        with tf.variable_scope("Generator"):
            tf.get_variable_scope().reuse_variables()

            if not self.labels_dim:
                # project `z` and reshape
                h0 = tf.reshape(linear(z, self.gf_dim*8*4*4, 'g_h0_lin'),
                                [-1, 4, 4, self.gf_dim * 8])
                h0 = lrelu(self.g_bn0(h0, train=False))

                h1 = deconv2d(h0, [self.batch_size, 8, 8, self.gf_dim*4], name='g_h1')
                h1 = lrelu(self.g_bn1(h1, train=False))

                h2 = deconv2d(h1, [self.batch_size, 16, 16, self.gf_dim*2], name='g_h2')
                h2 = lrelu(self.g_bn2(h2, train=False))

                h3 = deconv2d(h2, [self.batch_size, 32, 32, self.gf_dim*1], name='g_h3')
                h3 = lrelu(self.g_bn3(h3, train=False))

                h4 = deconv2d(h3, [self.batch_size, 64, 64, 3], name='g_h4')

                return tf.nn.tanh(h4)
            else:
                # yb = tf.expand_dims(tf.expand_dims(labels, 1),2)
                yb = tf.reshape(y, [self.batch_size, 1, 1, self.labels_dim])
                z = tf.concat(1, [z, y])

                h0 = lrelu(self.g_bn0(linear(z, self.gfc_dim, 'g_h0_lin'), train=False))
                h0 = tf.concat(1, [h0, y])

                h1 = lrelu(self.g_bn1(linear(h0, self.gf_dim * 4 * 32 * 32, 'g_h1_lin'), train=False))
                h1 = tf.reshape(h1, [self.batch_size, 32, 32, self.gf_dim * 4])
                h1 = conv_cond_concat(h1, yb)

                # h2 = tf.nn.relu(self.g_bn2(deconv2d(h1, [self.batch_size, 64, 64, self.gf_dim], name='g_h2'), train=False))
                h2 = deconv2d(h1, [self.batch_size, 64, 64, self.gf_dim], name='g_h2')
                h2 = self.g_bn2(h2, train=False)
                h2 = lrelu(h2)
                h2 = conv_cond_concat(h2, yb)

                # h2 = tf.nn.relu(self.g_bn2(linear(h1, self.gf_dim * 4 * 32 * 32, 'g_h2_lin'), train=False))
                # h2 = tf.reshape(h2, [self.batch_size, 32, 32, self.gf_dim * 4])
                # h2 = conv_cond_concat(h2, yb)
                #
                # h3 = tf.nn.relu(self.g_bn3(deconv2d(h2, [self.batch_size, 64, 64, self.gf_dim], name='g_h3'), train=False))
                # h3 = conv_cond_concat(h3, yb)

                return tf.nn.sigmoid(deconv2d(h2, [self.batch_size, 128, 128, 1], name='g_h3'))

                # # yb = tf.reshape(y, [-1, 1, 1, self.labels_dim])
                # yb = tf.reshape(y, [self.batch_size, 1, 1, self.labels_dim])
                # z = tf.concat(1, [z, y])
                #
                # h0 = tf.nn.relu(self.g_bn0(linear(z, self.gfc_dim, 'g_h0_lin')))
                # h0 = tf.concat(1, [h0, y])
                #
                # h1 = tf.nn.relu(self.g_bn1(linear(z, self.gf_dim*2*16*16, 'g_h1_lin'), train=False))
                # h1 = tf.reshape(h1, [self.batch_size, 16, 16, self.gf_dim * 2])
                # # h1 = tf.nn.relu(
                # #     self.g_bn1(
                # #         linear(z, self.gf_dim*2*((self.image_height + 3) // 4)*((self.image_width + 3) // 4), 'g_h1_lin'), train=False))
                # # h1 = tf.reshape(h1, [self.batch_size, (self.image_height + 3) // 4, (self.image_width + 3) // 4, self.gf_dim * 2])
                # h1 = conv_cond_concat(h1, yb)
                #
                # h2 = tf.nn.relu(self.g_bn2(deconv2d(h1, [self.batch_size, 32, 32, self.gf_dim * 2], name='g_h2'), train=False))
                # h2 = conv_cond_concat(h2, yb)
                #
                # return tf.nn.sigmoid(deconv2d(h2, [self.batch_size, self.image_height, self.image_width, self.c_dim], name='g_h3'))

    def load_mnist(self):
        data_dir = os.path.join("./data", self.dataset_name)
        
        fd = open(os.path.join(data_dir,'train-images-idx3-ubyte'))
        loaded = np.fromfile(file=fd,dtype=np.uint8)
        trX = loaded[16:].reshape((60000,28,28,1)).astype(np.float)

        fd = open(os.path.join(data_dir,'train-labels-idx1-ubyte'))
        loaded = np.fromfile(file=fd,dtype=np.uint8)
        trY = loaded[8:].reshape((60000)).astype(np.float)

        fd = open(os.path.join(data_dir,'t10k-images-idx3-ubyte'))
        loaded = np.fromfile(file=fd,dtype=np.uint8)
        teX = loaded[16:].reshape((10000,28,28,1)).astype(np.float)

        fd = open(os.path.join(data_dir,'t10k-labels-idx1-ubyte'))
        loaded = np.fromfile(file=fd,dtype=np.uint8)
        teY = loaded[8:].reshape((10000)).astype(np.float)

        trY = np.asarray(trY)
        teY = np.asarray(teY)
        
        X = np.concatenate((trX, teX), axis=0)
        y = np.concatenate((trY, teY), axis=0)
        
        seed = 547
        np.random.seed(seed)
        np.random.shuffle(X)
        np.random.seed(seed)
        np.random.shuffle(y)
        
        y_vec = np.zeros((len(y), self.labels_dim), dtype=np.float)
        for i, label in enumerate(y):
            y_vec[i,y[i]] = 1.0
        
        return X/255.,y_vec
            
    def save(self, checkpoint_dir, step):
        model_name = "DCGAN.model"
        model_dir = "%s_%s" % (self.dataset_name, self.batch_size)
        checkpoint_dir = os.path.join(checkpoint_dir, model_dir)

        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)

        self.saver.save(self.sess,
                        os.path.join(checkpoint_dir, model_name),
                        global_step=step)

    def load(self, checkpoint_dir):
        print(" [*] Reading checkpoints...")
        return False
        model_dir = "%s_%s" % (self.dataset_name, self.batch_size)
        checkpoint_dir = os.path.join(checkpoint_dir, model_dir)
        print checkpoint_dir

        # ckpt = tf.train.get_checkpoint_state(checkpoint_dir, "DCGAN.model-2")
        # print ckpt
        # print ckpt.model_checkpoint_path
        # if ckpt and ckpt.model_checkpoint_path:
        #     ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
        self.saver.restore(self.sess, os.path.join(checkpoint_dir, "DCGAN.model-2"))
        return True
        # else:
        #     return False

    def sobel(self, images):
        sobel_x = tf.constant([[-3, 0, 3], [-10, 0, 10], [-3, 0, 3]], tf.float32)
        sobel_x_filter = tf.reshape(sobel_x, [3, 3, 1, 1])
        sobel_y_filter = tf.transpose(sobel_x_filter, [1, 0, 2, 3])

        filtered_x = tf.nn.conv2d(images, sobel_x_filter,
                                  strides=[1, 1, 1, 1], padding='SAME')
        filtered_y = tf.nn.conv2d(images, sobel_y_filter,
                                  strides=[1, 1, 1, 1], padding='SAME')

        sobel_image = tf.sqrt(tf.add(tf.square(filtered_x), tf.square(filtered_y)))

        return sobel_image
