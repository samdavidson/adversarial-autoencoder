import tensorflow as tf
import Definitions

__author__ = 'zach'
__copyright__ = 'Copyright 2016'
__maintainer__ = 'Zach Bessinger'
__email__ = 'zach@cs.uky.edu'


class MultiDataset(object):
    """
    Loads two queues. One for training and one for testing and does batching
    accordingly.
    """

    def __init__(self, train_csv, test_csv, im_shape, batch_size, name="",
                 center_crop=None, flags=None):
        self.train_csv = train_csv
        self.test_csv = test_csv
        self.im_shape = im_shape
        self.batch_size = batch_size
        self.name = name
        self.center_crop = center_crop
        self.flags = flags

    def read_image(self, im_name, channels=3, im_format="jpg",
                   normalize=True, resize=None):
        """ Read an image from file using Tensorflow

        :param im_name: str: filename of image
        :param channels: int: number of channels in image
        :param im_format: str: must be jpg, jpeg, or jpg
        :param normalize: bool: scale between 0-1
        :param resize: list: in order of H x W
        :return: Image tensor
        """
        with tf.name_scope('read_image'):
            im_content = tf.read_file(im_name)
            im = None

            if im_format.lower() in ["jpg", "jpeg"]:
                im = tf.image.decode_jpeg(im_content, channels=channels)
            elif im_format.lower() == "png":
                im = tf.image.decode_png(im_content, channels=channels)
            else:
                raise ValueError("unrecognized format: %s" % im_format)

            if normalize:
                im = tf.cast(im, tf.float32) / 255.

            if resize:
                im = tf.image.resize_images(im, resize[0], resize[1],
                                            tf.image.ResizeMethod.BICUBIC)

            return im

    def load(self, num_epochs=None, is_training=True, reuse=False, seed=None):
        """
        Load a batch (be it just images or images and labels) using
        Tensorflow's multithreaded queueing system.

        :param num_epochs: int, Number of epochs to load. Default: None
        :param is_training: tf.placeholder(tf.bool), switch between reading
        training and testing CSVs
        :param reuse: Share the parameters. This is most likely deprecated.
        :param seed: Random seed for the input producer
        :return: Images, labels, and filenames from either the training or
        testing set depending upon the value of is_training
        """
        # with tf.variable_scope(self.name, reuse=reuse):
        with tf.device("/cpu:0"):
            # Switch between training and testing images
            with tf.name_scope('Train'):
                train_batch = self._make_batch(self.train_csv, 'Train')
            with tf.name_scope('Test'):
                test_batch = self._make_batch(self.test_csv, 'Test')
            return tf.cond(is_training,
                           lambda: train_batch,
                           lambda: test_batch)

    def load_train(self, num_epochs=None, is_training=True, reuse=False, seed=None):
        """
        Load a batch (be it just images or images and labels) using
        Tensorflow's multithreaded queueing system.

        :param num_epochs: int, Number of epochs to load. Default: None
        :param is_training: tf.placeholder(tf.bool), switch between reading
        training and testing CSVs
        :param reuse: Share the parameters. This is most likely deprecated.
        :param seed: Random seed for the input producer
        :return: Images, labels, and filenames from either the training or
        testing set depending upon the value of is_training
        """
        # with tf.variable_scope(self.name, reuse=reuse):
        with tf.device("/cpu:0"):
            # Switch between training and testing images
            with tf.name_scope('Train'):
                train_batch = self._make_batch(self.train_csv, 'Train')
        return train_batch

    def load_test(self, num_epochs=None, is_training=True, reuse=False, seed=None):
        """
        Load a batch (be it just images or images and labels) using
        Tensorflow's multithreaded queueing system.

        :param num_epochs: int, Number of epochs to load. Default: None
        :param is_training: tf.placeholder(tf.bool), switch between reading
        training and testing CSVs
        :param reuse: Share the parameters. This is most likely deprecated.
        :param seed: Random seed for the input producer
        :return: Images, labels, and filenames from either the training or
        testing set depending upon the value of is_training
        """
        # with tf.variable_scope(self.name, reuse=reuse):
        with tf.device("/cpu:0"):
            with tf.name_scope('Test'):
                test_batch = self._make_batch(self.test_csv, 'Test')
        return test_batch

    def _make_batch(self, train_csv, name):
        """
        Create a queue that outputs batches of images and labels
        """
        raise NotImplementedError('get_model must be implemented in subclass')


class PortraitsMultidataset(MultiDataset):
    def __init__(self, train_csv, test_csv, im_shape, batch_size, n_classes,
                 name="", center_crop=None, flags=None):
        """

        :param train_csv: str, filename of training data CSV
        :param test_csv: str, filename of testing data CSV
        :param im_shape: list, original shape of the images prior to
        cropping/scaling.
        :param batch_size: int, # of images to forward in one step
        :param n_classes: int, # of classes
        :param name: str, name of  this operation. Probably deprecated
        :param center_crop: list, Cropped dimensions. Default: none
        :param flags: Tensorflow flags
        """

        super(PortraitsMultidataset, self).__init__(train_csv, test_csv,
                                                    im_shape,
                                                    batch_size,
                                                    name=name,
                                                    center_crop=center_crop,
                                                    flags=flags)
        self.n_classes = n_classes

    def _make_batch(self, im_list, name, seed=None):
        print name, im_list
        with tf.device("/cpu:0"):
            reader = tf.TextLineReader()
            _, csv_content = reader.read(
                tf.train.string_input_producer([im_list],
                                               num_epochs=None,
                                               seed=seed,
                                               name=name))

            train_cols = tf.decode_csv(csv_content,
                                       record_defaults=Definitions.train_record_defaults, field_delim=',')

            # Pull information from file.
            train_file_name = Definitions.images_base + train_cols[0]
            print train_file_name

            train_cols[1] = tf.cast(train_cols[1], tf.float32)
            train_cols[1] = tf.floordiv(train_cols[1], 10.375)
            train_cols[1] = tf.cast(train_cols[1], tf.int32)
            train_scores = tf.one_hot(train_cols[1], depth=8)

            # train_scores = tf.one_hot(train_cols[1], depth=83)

            # Get image.
            train_image_data = Definitions.reader(train_file_name)

            # Batch everything together.
            return tf.train.shuffle_batch([train_file_name, train_scores, train_image_data],
                                          batch_size=self.batch_size,
                                          capacity=Definitions.capacity,
                                          min_after_dequeue=Definitions.min_after_dequeue,
                                          num_threads=Definitions.num_threads)
