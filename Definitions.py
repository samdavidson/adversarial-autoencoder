from __future__ import division
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import time
import re
import os

# Variables to set - listed in order of priority.
train_from_places = False
# These two may modify first_load and resume_training
load_from_checkpoint = True
load_from_places = True  # Does not matter if load_from_checkpoint is False
# May modify resume_training
first_load = True  # May be auto-set to False
# If True, the model is copied instead of being rebuilt.
resume_training = False  # May be auto-set to False

# File Paths
user_data_prefix = '/u/vul-d1/scratch/sam/portraits'
data_path = '/u/eag-d1/data/portraits/cleaned_faces/'
images_base = '/u/eag-d1/data/portraits/cleaned_faces/'
time_started = time.strftime('%Y%m%d/%H%M%S')
log_path = '%s/tensorboard_summaries/%s/' % (user_data_prefix, time_started)
checkpoint_path = '%s/results/%s/' % (user_data_prefix, time_started)
summaries_path = checkpoint_path + 'summaries/'
if not os.path.isdir(checkpoint_path):
    os.makedirs(checkpoint_path)
if not os.path.isdir(summaries_path):
    os.makedirs(summaries_path)

# Checkpoint Variables
checkpoint_time = '20160819/181757/'
checkpoint_step = '200000'
prev_checkpoint_dir = '%s/results/%s' % (user_data_prefix, checkpoint_time)
steps_between_checkpoints = 10
if train_from_places is True:
    steps_between_checkpoints = 10000

# Batch Variables
batch_size = 64
num_threads = 4
min_after_dequeue = 100
capacity = 10 * min_after_dequeue + 3 * batch_size

# Learning Rate Variables
starter_learning_rate = 0.01
decay_steps = 1000
decay_rate = 0.9
staircase = False
if load_from_places is True:
    layer_rate = {
        'Network/conv_layer1.*': .000001,
        'Network/block_0.*': .00001,
        'Network/block_1.*': .0001,
        'Network/block_2.*': .001,
        'Network/block_3.*': .01,
        'Network/fully_connected.*': .01
    }
else:
    layer_rate = {
        'Network/conv_layer1.*': .01,
        'Network/block_0.*': .01,
        'Network/block_1.*': .01,
        'Network/block_2.*': .01,
        'Network/block_3.*': .01,
        'Network/fully_connected.*': .01
    }

# How many batches to run.
train_steps = 200000
test_batches = (1667 // batch_size) + 1
figures_to_save = 5

# Gpu Options
allow_growth = True
use_gpu_fraction = True
per_process_gpu_memory_fraction = 0.25

# Record defaults for import_csv
record_defaults = [['']]
# noinspection PyTypeChecker
record_defaults.extend([[0.0] for _ in range(40)])
# noinspection PyTypeChecker
record_defaults.extend([[0] for __ in range(40)])

# Record defaults for places import_csv
train_record_defaults = [[''], [-1]]
test_record_defaults = [['']]


# Specific places variables

# 'standard' or 'challenge'
standard_or_challenge = 'standard'

if train_from_places is True:
    transient_labels_path = '/u/eag-d1/data/places_365/%s/categories_places365.txt' % standard_or_challenge
    transient_images_base = '/u/eag-d1/data/places_365/%s/data' % standard_or_challenge
    transient_annotations_path = '/u/eag-d1/data/places_365/%s/places365_val.txt' % standard_or_challenge

    # Attribute List
    with open(transient_labels_path) as f:
        attributes = [attr.split(' ')[0] for attr in f.readlines()]

# File Paths
# file_location category
transient_data_path_train = '/u/eag-d1/data/places_365/%s/shuffled_places365_train_%s.txt' % \
                            (standard_or_challenge, standard_or_challenge)
transient_data_path_val = '/u/eag-d1/data/places_365/%s/places365_val.txt' % standard_or_challenge
# file_location...
transient_data_path_test = '/u/eag-d1/data/places_365/%s/places365_test.txt' % standard_or_challenge


# Helper function for plot_information.
def to_colors(values):
    cols = []
    for i in range(len(values)):
        if values[i] <= .2:
            color = 'Red'
        elif values[i] < .8:
            color = 'Grey'
        else:
            color = 'Green'
        cols.append(color)
    return cols


def plot_information(image, computed, truth, path, name):
    if not os.path.isdir(path):
        os.makedirs(path)

    fig, ((image_plot, scatter), (computed_plot, truth_plot)) = plt.subplots(nrows=2, ncols=2)
    fig.set_size_inches(8.5, 11)

    # Image
    image_plot.imshow(image)
    image_plot.set_xticks([])
    image_plot.set_yticks([])

    # Scatter
    adj_truth = [(truth[i] / truth[i]) * (i + 1) - 1 for i in range(len(truth))]
    adj_computed = [(truth[i] - computed[i]) for i in range(len(computed))]
    scatter.scatter(adj_computed, adj_truth)
    scatter.plot([0, 0], [0, len(truth)])
    scatter.set_yticks(np.arange(40))
    scatter.set_yticklabels(attributes)
    scatter.set_ylabel('Attributes')
    scatter.set_xlabel('Truth - Computed')
    scatter.axes.set_autoscaley_on(False)
    scatter.axes.set_ybound(lower=0.0, upper=len(computed))
    scatter.axes.set_xbound(lower=-1, upper=1)
    scatter.set_title('Computed vs Truth')

    # Computed
    computed_plot.barh(np.arange(40), computed, color=to_colors(computed))
    computed_plot.set_yticks(np.arange(40))
    computed_plot.set_yticklabels(attributes)
    computed_plot.set_xlabel('Likelihood')
    computed_plot.axes.set_autoscalex_on(False)
    computed_plot.axes.set_xbound(lower=0.0, upper=1.0)
    computed_plot.set_title('Computed')

    # Truth
    truth_plot.barh(np.arange(40), truth, color=to_colors(truth))
    truth_plot.set_yticks(np.arange(40))
    truth_plot.set_yticklabels(attributes)
    truth_plot.set_xlabel('Likelihood')
    truth_plot.axes.set_autoscalex_on(False)
    truth_plot.axes.set_xbound(lower=0.0, upper=1.0)
    truth_plot.set_title('Ground Truth')

    plt.tight_layout()
    plt.ioff()
    plt.savefig(path+name, bbox_inches='tight', dpi='figure', format='svg')
    plt.close(fig)


def reader(file_name, image_height=186, image_width=171):
    image_data = tf.read_file(file_name)
    image_data = tf.image.decode_png(image_data, channels=1)

    image_data = tf.image.resize_images(image_data, 128, 128, tf.image.ResizeMethod.BICUBIC)

    # image_data = tf.reshape(image_data, [64, 64, 1])
    image_data = tf.to_float(image_data)
    image_data = tf.div(image_data, 255)

    return image_data


def get_train_ops(var_list, regex_dict, loss, global_step, optimizer=None):
    vars_list = list()
    opts_list = list()

    for regex in regex_dict:
        vars_list.append([v for v in var_list if re.match(regex, v.name) is not None])
        if optimizer is None:
            opts_list.append(tf.train.GradientDescentOptimizer(regex_dict[regex]))
        elif optimizer is "Adam":
            opts_list.append(tf.train.AdamOptimizer(learning_rate=regex_dict[regex]))

    grads = tf.gradients(loss, [v for v_list in vars_list for v in v_list])

    start = 0
    grads_list = list()
    for vs in vars_list:
        grads_list.append(grads[start:start + len(vs)])
        start += len(vs)

    train_ops = list()
    global_set = False
    for i in range(len(grads_list)):
        try:
            if global_set is False:
                train_ops.append(opts_list[i].apply_gradients(zip(grads_list[i], vars_list[i]),
                                                              global_step=global_step))
                global_set = True
            else:
                train_ops.append(opts_list[i].apply_gradients(zip(grads_list[i], vars_list[i])))
        except Exception as e:
            print str(e)

    return train_ops


def create_regexes(values, global_step):
    regexes = dict()
    for name in values:
        regexes[name] = tf.train.exponential_decay(values[name], global_step, decay_steps, decay_rate,
                                                   staircase=staircase)
    return regexes

# Options that should not be changed...
# Don't change this one. Change the two above it.
checkpoint_location = user_data_prefix + '/results/' + checkpoint_time + 'checkpoint-' + checkpoint_step
# Don't change this - resume_training must be false if load_from_checkpoint is false.
if load_from_checkpoint is False:
    resume_training = False
elif load_from_checkpoint is True:
    if load_from_places is False:
        first_load = False  # If you're not loading from places, it cannot be the first load.
    elif first_load is True:
        resume_training = False  # Have to make a new model.

